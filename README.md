# Распределённые объектные технологии
Для работы необходимо

 - Java
 - Groovy 2.3.0 
 - Определить GROOVY_HOME
 - Ant

###Task 1 (Client-Server)
Ограничение - 10 подключений

####Build

- `ant -f SocketsTCP/sockets-tcp.xml`


####Run server:

 - `java -jar SocketsTCP/out/artifacts/server.jar`

####Run clients

 - `java -jar SocketsTCP/out/artifacts/client.jar`
 - `java -jar SocketsTCP/out/artifacts/client.jar`
... 
-----------------------