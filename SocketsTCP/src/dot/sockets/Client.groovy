package dot.sockets

/**
 * Created by ipolotsky on 5/7/14.
 */
class Client {

    def static void main( def args ) {

        def limit = 1000
        def random = new Random()

        try {
            def socket = new Socket("127.0.0.1", Server.port)
            log("connected to server on port $socket.port")

            def inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()))
            def outStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))

            def string = random.nextInt(limit) + ' ' + random.nextInt(limit)
            log("request is '$string'")

            outStream.writeLine(string)
            outStream.flush()

            def line = inStream.readLine()
            log("answer is $line")

            socket.close()
        }
        catch (Exception e) {
            log("error $e.message")
        }

    }

    def static log(def message) {
        println("Client: $message")
    }

}
