package dot.sockets

/**
 * Created by ipolotsky on 5/7/14.
 */
class Server {

    static def port = 25000

    def static void main( def args ) {

        try {

            def serverSocket = new ServerSocket(port)
            log("started on port $port")

            (1..10).each {
                def socket = serverSocket.accept();
                log("new client (number $it)")

                def connection = new ClientConnection(
                        socket: socket,
                        number: it
                )

                new Thread(connection as Runnable).start()
            }

            log("limit of connections.")
        }
        catch (Exception e) {
            log("error $e.message")
        }


    }

    def static log(def message) {
        println("Server: $message")
    }

}
