package dot.sockets

/**
 * Created by ipolotsky on 5/7/14.
 */
class ClientConnection {

    def socket
    def number

    void run() {
        try {
            def inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()))
            def outStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))

            Server.log("wait request from $number")

            def answer = inStream.readLine().split(' ').collect {Integer.parseInt(it)}.sum()

            sleep(2000)

            outStream.writeLine(answer as String)
            outStream.flush();

            Server.log("send answer for $number")
        }
        catch (Exception e) {
            Server.log("client $number exception: $e.message")
        }

    }

}
